using UnityEngine;

public static class AppManager
{
    public static bool PlayMenuIntro { get; set; }

    public static CharacterData PlayerData { get; set; }

    public static MatchData EnemyData { get; set; }
}
