using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/CharacterData", order = 1)]
public class CharacterData : ScriptableObject
{
    public string Name;
    public Sprite thumbnail;
    public int unlockCost;
    public Vector3 stats;
    public GameObject prefab;
}
