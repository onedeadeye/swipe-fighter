using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/LeagueData", order = 1)]
public class LeagueData : ScriptableObject
{
    public MatchData[] League;
}
