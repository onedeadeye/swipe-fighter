using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/RosterData", order = 1)]
public class RosterData : ScriptableObject
{
    public CharacterData[] Roster;
}
