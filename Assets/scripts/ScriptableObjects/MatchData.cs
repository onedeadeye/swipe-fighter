using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/MatchData", order = 1)]
public class MatchData : ScriptableObject
{
    public string Name;
    public Sprite thumbnail;
    public GameObject prefab;
    public Vector3 boosts;
    public Enemy.EntityDifficulty difficulty;
}
