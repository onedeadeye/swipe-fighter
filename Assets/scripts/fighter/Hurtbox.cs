using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hurtbox : MonoBehaviour
{
    public Entity parent;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Hurt(float damage, Vector2 knockback, float flinch)
    {
        parent.TakeDamage(damage);
        if (knockback != Vector2.zero)
        {
            parent.TakeKnockback(knockback);
        }
        if (flinch != 0)
        {
            parent.TakeFlinch(flinch);
        }
    }
}
