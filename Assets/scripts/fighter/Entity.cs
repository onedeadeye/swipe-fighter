using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Entity : MonoBehaviour
{

    [SerializeField]
    protected string entityName = "DefaultEntity";

    public bool entityActive = false;

    protected bool isPlayer = false;

    public bool hasGravity = false;

    private float gravityScale = 2f;

    [SerializeField]
    private float floorYLevel = 0f;

    public float width;

    public float height;

    public bool Flipped { get; private set; }

    // live variables
    public bool Grounded
    {
        get { return hasGravity ? transform.position.y <= floorYLevel : true; }
    }

    private float pauseMoveTime = 0f;

    public bool PausedMovement
    {
        get { return Time.time < pauseMoveTime; }
    }

    private float forceAirMoveTime = 0f;

    public bool ForceAirMove
    {
        get { return Time.time < forceAirMoveTime; }
    }

    private float invulerableEndTime = 0f;

    public bool Invulnerable
    {
        get { return Time.time < invulerableEndTime; }
    }

    private float unstunTime = 0f;

    public bool Stunned
    {
        get { return Time.time < unstunTime; }
    }

    public bool Dead
    {
        get { return currentHealth <= 0; }
    }

    protected bool hasDied = false;

    [SerializeField]
    private float verticalMomentum = 0f;
    [SerializeField]
    private float horizontalMomentum = 0f;

    public float maxHealth = 100f;

    public float currentHealth = 0f;

    // ATK, AGI, DEF
    public Vector3 boosts = Vector3.zero;

    private float minStunDamage = 10f;
    private float minStunTime = 0.5f;

    [SerializeField]
    protected float moveSpeed;
    private float moveDeadzone = 0.1f;
    [SerializeField]
    protected float moveTarget;
    private bool atMoveTarget = false;
    protected bool canMove = true;

    [SerializeField]
    protected float jumpForce;

    [SerializeField]
    private AudioClip[] attackSounds;

    // component references
    protected Animator animator;
    protected Rigidbody2D rb;
    protected SpriteRenderer graphics;
    protected AudioSource audioSource;

    // Note because I confused myself: idleState is exposed so animations can control it.
    [SerializeField]
    protected EntityIdler idler;
    private bool isCleared = false;
    public bool usedAirMove = false;

    protected TouchInputManager.SwipeDirection comboDirection = TouchInputManager.SwipeDirection.None;

    // Start is called before the first frame update
    protected void EntityStart()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        isPlayer = gameObject.CompareTag("Player");

        graphics = GetComponentInChildren<SpriteRenderer>();
        audioSource = GetComponent<AudioSource>();
    }

    protected void EntityUpdate()
    {
        if (idler.idleState)
        {
            if (!isCleared)
            {
                comboDirection = TouchInputManager.SwipeDirection.None;
                isCleared = true;
                canMove = true;
            }
        }
        else
        {
            isCleared = false;
            canMove = false;
        }

        if (Grounded)
        {
            usedAirMove = false;
        }
    }

    protected void EntityFixedUpdate()
    {
        // Visual update segment
        if (Flipped)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        if (Invulnerable)
        {
            graphics.color = new Color(0.5f, 0.8f, 1f, 1f);
        }
        else if (Stunned)
        {
            graphics.color = new Color(0.75f, 0.75f, 0.75f, 1f);
        }
        else
        {
            graphics.color = Color.white;
        }

        // stunned, blocking

        // Movement update segment
        Vector3 finalMove = transform.position;

        if (!Grounded)
        {
            // MAKE THIS FLOAT COME FROM SOMEWHERE CENTRALIZED
            if (transform.position.x > 5f)
            {
                horizontalMomentum = -horizontalMomentum;
                horizontalMomentum -= 1f;
            }
            else if (transform.position.x < -5f)
            {
                horizontalMomentum = -horizontalMomentum;
                horizontalMomentum += 1f;
            }
        }
        else
        {
            if (transform.position.x > 5f)
            {
                SetMoveTarget(4f);
            }
            else if (transform.position.x < -5f)
            {
                SetMoveTarget(-4f);
            }
        }


        if (hasGravity && (!PausedMovement || Grounded))
        {
            float gravityDelta = gravityScale * 9.8f * Time.deltaTime;

            if (transform.position.y < floorYLevel)
            {
                Vector3 groundedPosition = transform.position;
                groundedPosition.y = floorYLevel;
                finalMove = groundedPosition;
                verticalMomentum = 0f;
                horizontalMomentum = 0f;
            }
            else if (transform.position.y > floorYLevel)
            {
                float yChange = 0f;
                float xChange = 0f;

                yChange = verticalMomentum * Time.deltaTime;
                verticalMomentum -= gravityDelta;

                xChange = horizontalMomentum * Time.deltaTime;

                Vector3 newPosition = transform.position;
                newPosition.y += yChange;
                newPosition.x += xChange;
                finalMove = newPosition;
            }
        }

        animator.SetBool("grounded", Grounded);
        animator.SetBool("stunned", Stunned);
        animator.SetBool("acting", PausedMovement || ForceAirMove);

        // moveSpeed is used as a check for if the entity can self-propel.
        // If this value is ever changed, this code may break.
        // There was a "|| Grounded" in this if parentized with !Acting, but it was causing a bug. It may need to go back.
        if (canMove && entityActive && !atMoveTarget && !PausedMovement && moveSpeed > 0f)
        {
            int moveDirection = 0;
            if (transform.position.x > moveTarget + moveDeadzone) moveDirection = -1;
            else if (transform.position.x < moveTarget - moveDeadzone) moveDirection = 1;
            else atMoveTarget = true;

            Vector3 moveVector = Vector3.zero;
            moveVector.x = moveSpeed * moveDirection * Time.deltaTime * ((boosts.y / 100) + 1);

            if (moveDirection != 0)
            {
                finalMove += moveVector;
                animator.SetBool("move", true);
            }
            else
            {
                animator.SetBool("move", false);
            }
        }
        else
        {
            animator.SetBool("move", false);
        }

        if (finalMove != transform.position)
        {
            rb.MovePosition(finalMove);
        }

        if (Dead && !hasDied)
        {
            animator.SetBool("dead", true);

            if (isPlayer)
            {
                TakeKnockback(new Vector2(1, 2));
            }
            else
            {
                TakeKnockback(new Vector2(3, 5));
            }

            hasDied = true;

            GameManager.Instance.AlertWin(!isPlayer);
        }
    }

    // sets position differently for player and enemy
    public void Reset()
    {
        currentHealth = maxHealth;
        animator.SetBool("dead", false);
        hasDied = false;

        transform.localPosition = new Vector3(isPlayer ? -3f : 0f, 0, 0);
        moveTarget = transform.position.x;
    }

    public void SetFlipped(bool flipped)
    {
        Flipped = flipped;
    }

    public void PlayAttackSound()
    {
        audioSource.PlayOneShot(attackSounds[Random.Range(0, attackSounds.Length)]);
    }

    private void OnDrawGizmos()
    {
        Color gizmoColor = Color.blue;
        gizmoColor.a = 0.25f;
        Gizmos.color = gizmoColor;

        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);

        Vector3 newPosition = Vector3.zero;
        newPosition.y += height / 2;

        Gizmos.DrawWireCube(newPosition, new Vector3(width, height, 1));
    }

    public void AddVerticalMomentum(float momentum)
    {
        verticalMomentum = 0f;
        verticalMomentum += momentum;
    }

    public void AddHorizontalMomentum(float momentum)
    {
        horizontalMomentum = 0f;
        horizontalMomentum += momentum * (Flipped ? 1 : -1);
    }

    public void AddInvulnerableTime()
    {
        if (!Invulnerable && idler.idleState)
        {
            invulerableEndTime = Time.time + 0.5f + (0.5f * (boosts.y / 100));
        }
    }

    public void GenericAct(float duration = 0.5f, bool hangTime = true)
    {
        if (Stunned) return;

        // probably should make this a class member later
        if (Grounded) pauseMoveTime = Time.time + duration;
        else
        {
            if (hangTime)
            {
                pauseMoveTime = Time.time + duration;
            }
            else
            {
                forceAirMoveTime = Time.time + duration;
            }
        }
    }

    public void Jump()
    {
        if (Grounded)
        {
            transform.Translate(new Vector3(0, 0.1f, 0));
            AddVerticalMomentum(jumpForce);
        }
    }

    public void TakeDamage(float damage)
    {
        if (Invulnerable) return;

        float damageMultiplier = 1 + ((50 * Mathf.Log((1 / 10) * boosts.z + 1)) / 100);
        float finalDamage = damage * damageMultiplier;
        currentHealth -= finalDamage;

        if (damage >= minStunDamage)
        {
            unstunTime = Time.time + ((damage / minStunDamage) * minStunTime);
        }

        GameManager.Instance.UpdateHealthDisplays();
        GameManager.Instance.AlertDamage(damage, isPlayer);
    }

    public void TakeKnockback(Vector2 knockback)
    {
        if (Invulnerable) return;

        // Airborne entities take half momentum
        if (!Grounded)
        {
            knockback.y *= 0.5f;
        }

        transform.Translate(new Vector3(0, 0.1f, 0));
        AddVerticalMomentum(knockback.y);
        AddHorizontalMomentum(knockback.x);
    }

    public void TakeFlinch(float flinch)
    {
        if (Invulnerable) return;

        Vector3 flinchMove = transform.position;
        flinchMove.x += (flinch * (Flipped ? 1 : -1));
        transform.position = flinchMove;
    }

    public void SetMoveTarget(float newTarget)
    {
        moveTarget = newTarget;
        atMoveTarget = false;
    }
}
