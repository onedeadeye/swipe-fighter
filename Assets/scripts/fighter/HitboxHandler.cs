using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxHandler : MonoBehaviour
{
    [SerializeField]
    private Hitbox[] hitboxes;

    public Entity host;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DisableAttack()
    {
        GameManager.Instance.AlertAttack(true);
        foreach (Hitbox h in hitboxes)
        {
            h.active = false;
        }
    }
}
