using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Player : Entity
{
    // This is used to alert the enemy to react to the player's input
    public static event Action<TouchInputManager.SwipeDirection> OnMove = delegate { };

    // structs
    protected struct MoveMatrix
    {
        public EntityAction jabAction;
        public EntityAction launchAction;
        public EntityAction jumpAction;
        public EntityAction blockAction;
        public EntityAction westAction;
        public EntityAction duckAction;
        public EntityAction southAction;
        public EntityAction knockdownAction;
    }

    protected struct AirMoveMatrix
    {
        public EntityAction jabAction;
        public EntityAction downAction;
        public EntityAction spikeAction;
    }

    protected MoveMatrix moves;
    protected AirMoveMatrix airMoves;

    protected bool canBuffer = false;
    private bool hasBuffered = false;
    //[HideInInspector]

    // Start is called before the first frame update
    void Start()
    {
        EntityStart();

        Reset();

        moves.jabAction = new PlayerLoopAction("jab", this, animator, TouchInputManager.SwipeDirection.East);
        moves.launchAction = new PlayerAction("launch", this, animator);
        moves.jumpAction = new JumpAction("jump", this);
        moves.blockAction = new BlockAction("block", this, animator);
        moves.duckAction = new AnimatedAction("duck", this, animator);
        moves.knockdownAction = new PlayerAction("knockdown", this, animator);

        airMoves.jabAction = new AirborneAction("jab", this, animator, 0.5f, true);
        airMoves.downAction = new AirborneAction("duck", this, animator, 0.5f, false);
        airMoves.spikeAction = new AirborneAction("knockdown", this, animator, 0.5f, true);

        TouchInputManager.OnSwipe += ConvertInputToAction;
    }

    // Update is called once per frame
    void Update()
    {
        EntityUpdate();

        if (!idler.idleState && !hasBuffered)
        {
            float currentAnimLength = animator.GetCurrentAnimatorStateInfo(0).length;
            // magic number that should be a class member
            float canBufferNormalizedTime = (currentAnimLength - 0.35f) / currentAnimLength;

            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= canBufferNormalizedTime)
            {
                canBuffer = true;
            }
            else
            {
                canBuffer = false;
            }
        }
        else
        {
            canBuffer = false;

            if (idler.idleState)
            {
                hasBuffered = false;
            }
        }
    }

    void FixedUpdate()
    {
        EntityFixedUpdate();
    }

    new public void SetFlipped(bool flipped)
    {
        base.SetFlipped(flipped);

        TouchInputManager.PlayerFlipped = Flipped;
    }

    private void ConvertInputToAction(TouchInputManager.SwipeDirection direction)
    {
        if (Stunned)
        {
            return;
        }

        if (!entityActive)
        {
            return;
        }

        if (!Grounded && usedAirMove)
        {
            // One move per jump, buster
            return;
        }

        // Special case to prevent buffering a jump action
        if (direction == TouchInputManager.SwipeDirection.North && Grounded && !idler.idleState) { return; }

        OnMove.Invoke(direction);

        if (idler.idleState || canBuffer || direction == comboDirection)
        {
            if (Grounded)
            {
                switch (direction)
                {
                    case TouchInputManager.SwipeDirection.East:
                        moves.jabAction.Act();
                        break;
                    case TouchInputManager.SwipeDirection.NorthEast:
                        moves.launchAction.Act();
                        break;
                    case TouchInputManager.SwipeDirection.North:
                        moves.jumpAction.Act();
                        break;
                    case TouchInputManager.SwipeDirection.NorthWest:
                        if (canBuffer) break;
                        moves.blockAction.Act();
                        break;
                    case TouchInputManager.SwipeDirection.West:
                        // Both dodge move cases are incompatible with buffering because of how invulnerability works
                        if (canBuffer) break;
                        moves.blockAction.Act();
                        break;
                    case TouchInputManager.SwipeDirection.SouthWest:
                        moves.duckAction.Act();
                        break;
                    case TouchInputManager.SwipeDirection.South:
                        moves.duckAction.Act();
                        break;
                    case TouchInputManager.SwipeDirection.SouthEast:
                        moves.knockdownAction.Act();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (direction)
                {
                    case TouchInputManager.SwipeDirection.East:
                        airMoves.jabAction.Act();
                        break;
                    case TouchInputManager.SwipeDirection.SouthWest:
                        airMoves.downAction.Act();
                        break;
                    case TouchInputManager.SwipeDirection.South:
                        airMoves.downAction.Act();
                        break;
                    case TouchInputManager.SwipeDirection.SouthEast:
                        airMoves.spikeAction.Act();
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void SetComboDirection(TouchInputManager.SwipeDirection comboDirection)
    {
        this.comboDirection = comboDirection;
    }

    public void AlertBuffer()
    {
        if (canBuffer)
        {
            hasBuffered = true;
        }
    }
}
