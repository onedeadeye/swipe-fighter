using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Entity
{
    public enum EntityDifficulty
    {
        PASSIVE,
        EASY,
        NORMAL,
        HARD,
        IMPOSSIBLE
    }

    private struct MoveMatrix
    {
        public EntityAction jabAction;
        public EntityAction secondJabAction;
        public EntityAction launchAction;
        public EntityAction knockdownAction;
        public EntityAction jumpAction;
        public EntityAction blockAction;
        public EntityAction duckAction;
    }

    private MoveMatrix moves;

    public EntityDifficulty difficulty = EntityDifficulty.NORMAL;

    private Player enemyPlayer;

    private float nextRandomMoveTime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        EntityStart();

        Reset();

        moves.jabAction = new EnemyAction("jab", this, animator);
        moves.secondJabAction = new EnemyForceLoopAction("jab", this, animator);
        moves.launchAction = new EnemyAction("launch", this, animator);
        moves.knockdownAction = new EnemyAction("knockdown", this, animator);
        moves.jumpAction = new JumpAction("jump", this);
        moves.blockAction = new BlockAction("block", this, animator);
        moves.duckAction = new EnemyAction("duck", this, animator);

        Player.OnMove += ConvertMoveToResponse;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        EntityFixedUpdate();

        if (Dead) return;

        if (Time.time >= nextRandomMoveTime && difficulty != EntityDifficulty.PASSIVE)
        {
            EntityAction[] randomMoves = CreateMovesFromDifficulty();

            EntityAction chosenResponse = randomMoves[Random.Range(0, randomMoves.Length)];
            chosenResponse.Act();

            // This section sets up loop attacks
            if (chosenResponse == moves.jabAction)
            {
                int par = 5 - ((int)difficulty);
                if (Random.Range(0, 4) >= par)
                {
                    moves.secondJabAction.Act();
                }
            }

            SetNextRandomMoveTime();
        }
    }

    private void SetNextRandomMoveTime()
    {
        float randomMoveOffset = 0f;
        switch (difficulty)
        {
            case EntityDifficulty.PASSIVE:
                // Passive enemies do not act.
                break;
            case EntityDifficulty.EASY:
                randomMoveOffset = Random.Range(3f, 5f);
                break;
            case EntityDifficulty.NORMAL:
                randomMoveOffset = Random.Range(1f, 2f);
                break;
            case EntityDifficulty.HARD:
                randomMoveOffset = Random.Range(1f, 2f);
                break;
            case EntityDifficulty.IMPOSSIBLE:
                // yeah good luck buddy
                randomMoveOffset = Random.Range(0.5f, 1f);
                break;
            default:
                break;
        }

        nextRandomMoveTime = Time.time + randomMoveOffset;
    }

    private void ConvertMoveToResponse(TouchInputManager.SwipeDirection direction)
    {
        if (!entityActive) return;

        // Enemies are WITHOUT LIMITS.
        if (Grounded)
        {
            EntityAction[] responses = CreateResponsesFromDirection(direction);

            StartCoroutine("ContemplateResponse", responses);
        }
        else
        {
            // air moves go here
        }
    }

    // Enemies choose equally between all presented actions when deciding.
    private IEnumerator ContemplateResponse(EntityAction[] responses)
    {
        float waitDelay = 0f;

        switch (difficulty)
        {
            case EntityDifficulty.PASSIVE:
                // Passive enemies do not act.
                break;
            case EntityDifficulty.EASY:
                waitDelay = Random.Range(0.75f, 1f);
                break;
            case EntityDifficulty.NORMAL:
                waitDelay = Random.Range(0.4f, 0.7f);
                break;
            case EntityDifficulty.HARD:
                waitDelay = Random.Range(0.15f, 0.3f);
                break;
            case EntityDifficulty.IMPOSSIBLE:
                // yeah good luck buddy
                waitDelay = Random.Range(0.05f, 0.1f);
                break;
            default:
                break;
        }

        // Passive enemies do not act.
        if (difficulty == EntityDifficulty.PASSIVE) yield break;

        yield return new WaitForSeconds(waitDelay);

        // Stunned characters give up
        if (Stunned) { yield break; }

        EntityAction chosenResponse = responses[Random.Range(0, responses.Length)];
        chosenResponse.Act();

        // Reacting resets the random move time to lessen the unstoppable barrage from the enemy
        SetNextRandomMoveTime();
    }

    private EntityAction[] CreateMovesFromDifficulty()
    {
        List<EntityAction> responses = new List<EntityAction>();
        switch (difficulty)
        {
            case EntityDifficulty.PASSIVE:
                // Passive enemies do not act.
                break;
            case EntityDifficulty.EASY:
                responses.Add(moves.blockAction);
                responses.Add(moves.jumpAction);
                break;
            case EntityDifficulty.NORMAL:
                responses.Add(moves.blockAction);
                responses.Add(moves.jabAction);
                responses.Add(moves.jabAction);
                responses.Add(moves.launchAction);
                break;
            case EntityDifficulty.HARD:
                responses.Add(moves.jabAction);
                responses.Add(moves.launchAction);
                break;
            case EntityDifficulty.IMPOSSIBLE:
                // yeah good luck buddy
                responses.Add(moves.jabAction);
                responses.Add(moves.launchAction);
                responses.Add(moves.knockdownAction);
                break;
            default:
                break;
        }

        return responses.ToArray();
    }

    private EntityAction[] CreateResponsesFromDirection(TouchInputManager.SwipeDirection direction)
    {
        List<EntityAction> responses = new List<EntityAction>();
        switch (direction)
        {
            case TouchInputManager.SwipeDirection.East: // Jab
                responses.Add(moves.blockAction);
                break;
            case TouchInputManager.SwipeDirection.NorthEast: // Launch
                responses.Add(moves.duckAction);
                break;
            case TouchInputManager.SwipeDirection.North: // Jump
                responses.Add(moves.launchAction);
                break;
            case TouchInputManager.SwipeDirection.NorthWest: // BLock
                responses.Add(moves.blockAction);
                break;
            case TouchInputManager.SwipeDirection.West: // Block
                responses.Add(moves.blockAction);
                break;
            case TouchInputManager.SwipeDirection.SouthWest: // Duck
                responses.Add(moves.knockdownAction);
                break;
            case TouchInputManager.SwipeDirection.South: // Duck
                responses.Add(moves.knockdownAction);
                break;
            case TouchInputManager.SwipeDirection.SouthEast: // Knockdown
                responses.Add(moves.jumpAction);
                break;
            default:
                responses.Add(moves.blockAction);
                break;
        }

        return responses.ToArray();
    }
}
