using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedAction : EntityAction
{
    protected Animator animator;

    public AnimatedAction(string actionName, Entity entity, Animator animator) : base(actionName, entity)
    {
        this.animator = animator;
    }

    public override void Act()
    {
        entity.GenericAct();
        animator.SetTrigger(actionName);
    }

    public void Act(float duration, bool hangTime = true)
    {
        entity.GenericAct(duration, hangTime);
        animator.SetTrigger(actionName);
    }
}
