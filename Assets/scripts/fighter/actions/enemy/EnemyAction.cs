using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAction : AnimatedAction
{
    protected Enemy enemy;

    public EnemyAction(string actionName, Enemy enemy, Animator animator) : base(actionName, enemy, animator)
    {
        this.enemy = enemy;
    }

    public override void Act()
    {
        base.Act();
    }
}
