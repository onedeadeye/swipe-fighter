using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyForceLoopAction : EnemyAction
{
    public EnemyForceLoopAction(string actionName, Enemy enemy, Animator animator) : base(actionName, enemy, animator)
    {

    }

    public override void Act()
    {
        base.Act();
        animator.SetTrigger("forceLoop");
    }
}
