using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpAction : EntityAction
{
    public JumpAction(string actionName, Entity entity) : base(actionName, entity)
    {

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Act()
    {
        // in this case, its a jump
        entity.Jump();
    }
}
