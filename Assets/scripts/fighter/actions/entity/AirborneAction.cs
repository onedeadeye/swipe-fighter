using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirborneAction : AnimatedAction
{
    private float duration = 0f;
    private bool hangTime = false;

    public AirborneAction(string actionName, Entity entity, Animator animator, float duration, bool hangTime) : base(actionName, entity, animator)
    {
        this.duration = duration;
        this.hangTime = hangTime;
    }

    public override void Act()
    {
        base.Act(duration, hangTime);

        if (!entity.Grounded) entity.usedAirMove = true;
    }
}
