using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockAction : AnimatedAction
{
    public BlockAction(string actionName, Entity entity, Animator animator) : base(actionName, entity, animator)
    {

    }

    public override void Act()
    {
        base.Act();

        entity.AddInvulnerableTime();
    }
}
