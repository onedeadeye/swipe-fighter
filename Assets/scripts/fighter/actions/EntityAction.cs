using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EntityAction
{
    // Animators use strings for everything, so shall we!
    protected string actionName;
    protected Entity entity;

    public EntityAction(string actionName, Entity entity)
    {
        this.actionName = actionName;
        this.entity = entity;
    }

    public abstract void Act();
}
