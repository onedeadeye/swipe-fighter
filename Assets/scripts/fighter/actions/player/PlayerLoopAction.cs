using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLoopAction : PlayerAction
{
    TouchInputManager.SwipeDirection loopDirection = TouchInputManager.SwipeDirection.None;

    public PlayerLoopAction(string actionName, Player player, Animator animator, TouchInputManager.SwipeDirection loopDirection) : base(actionName, player, animator)
    {
        this.loopDirection = loopDirection;
    }

    public override void Act()
    {
        base.Act();
        player.SetComboDirection(loopDirection);
    }
}
