using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAction : AnimatedAction
{
    protected Player player;

    public PlayerAction(string actionName, Player player, Animator animator) : base(actionName, player, animator)
    {
        this.player = player;
    }

    public override void Act()
    {
        base.Act();

        player.AlertBuffer();
    }
}
