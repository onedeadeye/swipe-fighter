using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour
{
    [SerializeField]
    private HitboxHandler handler;

    [SerializeField]
    private Vector2 size = Vector2.zero;

    [SerializeField]
    private float damage = 0;

    [SerializeField]
    private Vector2 knockback = Vector2.zero;

    [SerializeField]
    private float flinch = 0f;

    ContactFilter2D newFilter;

    public bool active = false;

    // Start is called before the first frame update
    void Start()
    {
        newFilter.SetLayerMask(LayerMask.GetMask("Hurtbox"));
        newFilter.useTriggers = true;
    }

    void OnEnable()
    {
        active = true;
    }

    void OnDisable()
    {
        // If the collider is being disabled and we're still active, we missed the attack.
        if (active)
        {
            GameManager.Instance.AlertAttack(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!active) return;

        List<Collider2D> colliders = new List<Collider2D>();
        Physics2D.OverlapBox(transform.position, size, transform.rotation.y, newFilter, colliders);

        //Debug.Log(colliders.Count);

        foreach (Collider2D hit in colliders)
        {
            //Debug.Log(hit.name + " takes " + damage + " and " + knockback);
            if (!hit.CompareTag(tag))
            {
                //Debug.Log("hit enemy");
                Hurtbox hurt = hit.GetComponent<Hurtbox>();

                if (hurt.parent.Invulnerable) continue;

                hurt.Hurt(damage * (1 + (handler.host.boosts.x / 100)), knockback, flinch);

                Vector3 hitPosition = transform.position;
                hitPosition.x = (transform.position.x + hit.transform.position.x) / 2f;
                GameObject hitEffect = GameObject.Instantiate(GameManager.Instance.hitEffectPrefab, hitPosition, Quaternion.identity);
                Destroy(hitEffect, 0.1f);

                handler.DisableAttack();
                return;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Color gizmoColor = Color.red;
        gizmoColor.a = 0.25f;
        Gizmos.color = gizmoColor;

        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);

        Gizmos.DrawCube(Vector3.zero, new Vector3(size.x * 2, size.y * 2, 1));
    }
}
