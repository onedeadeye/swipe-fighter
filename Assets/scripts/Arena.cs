using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arena : MonoBehaviour
{
    public float EdgeDistance = 5f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnDrawGizmos()
    {
        Color gizmoColor = Color.green;
        gizmoColor.a = 0.25f;
        Gizmos.color = gizmoColor;

        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);

        Vector3 leftPosition = new Vector3(-(EdgeDistance + 3), 0, 0);
        Vector3 rightPosition = new Vector3(EdgeDistance + 3, 0, 0);

        Gizmos.DrawCube(leftPosition, new Vector3(6, 10, 1));
        Gizmos.DrawCube(rightPosition, new Vector3(6, 10, 1));
    }
}
