using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField]
    private AudioClip winHit;
    [SerializeField]
    private AudioClip loseHit;

    [SerializeField]
    private AudioClip winTrack;
    [SerializeField]
    private AudioClip loseTrack;

    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayWinHit()
    {
        if (!PlayerPrefManager.GetPlayMusic()) return;
        audioSource.PlayOneShot(winHit);
    }

    public void PlayLoseHit()
    {
        if (!PlayerPrefManager.GetPlayMusic()) return;
        audioSource.PlayOneShot(loseHit);
    }

    public void PlayWinTrack()
    {
        if (!PlayerPrefManager.GetPlayMusic()) return;
        audioSource.PlayOneShot(winTrack);
    }

    public void PlayLoseTrack()
    {
        if (!PlayerPrefManager.GetPlayMusic()) return;
        audioSource.PlayOneShot(loseTrack);
    }
}
