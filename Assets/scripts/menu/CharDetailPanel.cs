using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharDetailPanel : MonoBehaviour
{
    public Text charName;
    public Image mugshot;
    public Slider strength;
    public Slider agility;
    public Slider defense;
    public GameObject selectButton;
    public GameObject buyButton;
    public Text costText;
}
