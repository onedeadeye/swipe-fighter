using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectEnemyIcon : MonoBehaviour
{
    public Image thumbnail;
    public GameObject lockedIcon;
    public GameObject defeatedIcon;
}
