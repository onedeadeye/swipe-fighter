using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [HideInInspector]
    public static MenuManager instance;

    [Header("Data Links")]
    [SerializeField]
    private RosterData roster;
    [SerializeField]
    private LeagueData[] leagues;

    [Header("Header")]
    [SerializeField]
    private GameObject backButton;
    [SerializeField]
    private Text cashText;

    [Header("Home Menu")]
    [SerializeField]
    private GameObject homeMenu;
    [SerializeField]
    private GameObject characterPivot;

    [Header("Character Menu")]
    [SerializeField]
    private GameObject characterMenu;
    [SerializeField]
    private GameObject characterIconGrid;
    [SerializeField]
    private CharDetailPanel detailPanel;

    [SerializeField]
    private GameObject characterListEntryPrefab;

    [Header("Other Menus")]
    [SerializeField]
    private TrainingMenu trainingMenu;
    [SerializeField]
    private GameObject shopMenu;
    [SerializeField]
    private GameObject creditsMenu;

    [Header("Play Menu")]
    [SerializeField]
    private GameObject playMenu;
    [SerializeField]
    private GameObject enemyPanelScroll;
    [SerializeField]
    private CharDetailPanel enemyDetailPanel;

    [SerializeField]
    private GameObject matchListEntryPrefab;

    [Header("Settings Menu")]
    [SerializeField]
    private GameObject settingsMenu;

    [Header("Popups")]
    public BuyItemPopup buyItemPopup;
    [SerializeField]
    private GameObject addCashPopup;
    [SerializeField]
    private WaitingPopup waitingPopup;
    [SerializeField]
    private GameObject resetDataPopup;

    [Header("Effects")]
    [SerializeField]
    private ParticleSystem cashDropParticles;
    [SerializeField]
    private Animator wiperAnimation;

    private Animator animator;
    private AudioSource menuMusic;

    private int previewedCharacterIndex;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        animator = GetComponent<Animator>();
        menuMusic = GetComponent<AudioSource>();

        // temp command to unlock the Boxer
        PlayerPrefManager.UnlockCharacter(0);

        ReloadAllMenus();

        if (AppManager.PlayMenuIntro) animator.SetTrigger("playIntro");
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void ReloadAllMenus()
    {
        LoadCharacter();
        UpdateCashDisplay(false);
        GenerateCharacterMenu();
        GeneratePlayMenu();
        trainingMenu.UpdateUI();
    }

    public void SignalEndOfAnimations()
    {
        animator.enabled = false;
        menuMusic.enabled = PlayerPrefManager.GetPlayMusic();
    }

    private void GenerateCharacterMenu()
    {
        // Wipe any previous portraits
        foreach (Transform child in characterIconGrid.transform)
        {
            Destroy(child.gameObject);
        }

        // Populate select character screen
        for (int i = 0; i < roster.Roster.Length; i++)
        {
            int capturedIndex = i;

            GameObject newIcon = Instantiate(characterListEntryPrefab, characterIconGrid.transform);
            SelectCharIcon script = newIcon.GetComponent<SelectCharIcon>();
            script.thumbnail.sprite = roster.Roster[i].thumbnail;
            script.lockedIcon.SetActive(!PlayerPrefManager.GetUnlockedCharacter(i));
            Button button = newIcon.GetComponent<Button>();
            button.onClick.AddListener(() => ShowDetailsForFighter(capturedIndex));

            //Debug.Log(i + " " + PlayerPrefManager.GetSelectedCharacter());
            if (i == PlayerPrefManager.GetSelectedCharacter())
            {
                script.selectedIcon.SetActive(true);
            }
        }
    }

    // Method of Mass Destruction
    private void GeneratePlayMenu()
    {
        // Populate Leagues on the menu
        for (int k = 0; k < leagues.Length; k++)
        {
            int capturedLeagueIndex = k;

            for (int i = 0; i < leagues[k].League.Length; i++)
            {
                int capturedIndex = i;

                // THIS WILL BREAK IF PLAY MENU COMPONENTS EVER COME IN MORE THAN PAIRS OF 2
                GameObject newIcon = Instantiate(matchListEntryPrefab,
                                    enemyPanelScroll.transform.GetChild((k * 2) + 1));
                SelectEnemyIcon script = newIcon.GetComponent<SelectEnemyIcon>();
                script.thumbnail.sprite = leagues[k].League[i].thumbnail;
                // these need logic
                script.lockedIcon.SetActive(false);
                script.defeatedIcon.SetActive(false);
                Button button = newIcon.GetComponent<Button>();
                button.onClick.AddListener(() => ShowDetailsForEnemy(capturedLeagueIndex, capturedIndex));
            }
        }
    }

    public void ShowDetailsForFighter(int index)
    {
        CharacterData entry = roster.Roster[index];
        detailPanel.charName.text = entry.Name;
        detailPanel.mugshot.sprite = entry.thumbnail;
        detailPanel.strength.value = entry.stats.x;
        detailPanel.agility.value = entry.stats.y;
        detailPanel.defense.value = entry.stats.z;

        //Debug.Log(index + " " + PlayerPrefManager.GetUnlockedCharacter(index));
        if (PlayerPrefManager.GetUnlockedCharacter(index))
        {
            //Debug.Log("character unlocked");
            if (PlayerPrefManager.GetSelectedCharacter() != index)
            {
                //Debug.Log("character not selected");
                detailPanel.selectButton.SetActive(true);
                detailPanel.buyButton.SetActive(false);
                detailPanel.costText.gameObject.SetActive(false);
            }
        }
        else
        {
            //Debug.Log("character locked");
            detailPanel.selectButton.SetActive(false);
            detailPanel.buyButton.SetActive(true);
            detailPanel.costText.text = "$" + entry.unlockCost;
            detailPanel.costText.gameObject.SetActive(true);
        }

        previewedCharacterIndex = index;
    }

    public void ShowDetailsForEnemy(int enemyLeague, int enemyIndex)
    {
        MatchData entry = leagues[enemyLeague].League[enemyIndex];
        enemyDetailPanel.charName.text = entry.Name;
        enemyDetailPanel.mugshot.sprite = entry.thumbnail;
        enemyDetailPanel.strength.value = entry.boosts.x;
        enemyDetailPanel.agility.value = entry.boosts.y;
        enemyDetailPanel.defense.value = entry.boosts.z;

        Button button = enemyDetailPanel.selectButton.GetComponent<Button>();
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => StartMatch(enemyLeague, enemyIndex));
        button.gameObject.SetActive(true);
    }

    public void SelectCharacter()
    {
        characterIconGrid.transform.GetChild(PlayerPrefManager.GetSelectedCharacter()).GetComponent<SelectCharIcon>().selectedIcon.SetActive(false);
        PlayerPrefManager.SetSelectedCharacter(previewedCharacterIndex);
        detailPanel.selectButton.SetActive(false);
        characterIconGrid.transform.GetChild(PlayerPrefManager.GetSelectedCharacter()).GetComponent<SelectCharIcon>().selectedIcon.SetActive(true);
        LoadCharacter();
    }

    public void BuyCharacter()
    {
        CharacterData charEntry = roster.Roster[previewedCharacterIndex];

        if (PlayerPrefManager.GetCash() < charEntry.unlockCost)
        {
            // we don't have enough for this character
            LoadAddCashPopup();
        }
        else
        {
            // purchasing logic goes here
            buyItemPopup.PromptItem(charEntry.Name, charEntry.thumbnail, charEntry.unlockCost, CompleteBuyCharacter);
        }
    }

    public void CompleteBuyCharacter()
    {
        // probably add a flourish here
        CharacterData charEntry = roster.Roster[previewedCharacterIndex];
        PlayerPrefManager.LoseCash(charEntry.unlockCost);
        PlayerPrefManager.UnlockCharacter(previewedCharacterIndex);
        UpdateCashDisplay(false);
        GenerateCharacterMenu();
        ShowDetailsForFighter(previewedCharacterIndex);
    }

    private void LoadCharacter()
    {
        if (characterPivot.transform.childCount > 0)
        {
            Destroy(characterPivot.transform.GetChild(0).gameObject);
        }

        GameObject newCharacter = Instantiate(roster.Roster[PlayerPrefManager.GetSelectedCharacter()].prefab,
            characterPivot.transform);
        newCharacter.transform.localScale = new Vector2(300, 300);
    }

    public void UpdateCashDisplay(bool playEffect)
    {
        cashText.text = "$" + PlayerPrefManager.GetCash();
        if (playEffect)
        {
            cashDropParticles.Play();
        }
    }

    private void CloseAllMenus()
    {
        backButton.SetActive(true);

        homeMenu.SetActive(false);
        characterMenu.SetActive(false);
        trainingMenu.gameObject.SetActive(false);
        shopMenu.SetActive(false);
        settingsMenu.SetActive(false);
        playMenu.SetActive(false);
        creditsMenu.SetActive(false);

        addCashPopup.SetActive(false);
        resetDataPopup.SetActive(false);
    }

    public void LoadHomeMenu()
    {
        CloseAllMenus();
        backButton.SetActive(false);
        homeMenu.SetActive(true);
    }

    public void LoadCharacterMenu()
    {
        CloseAllMenus();
        characterMenu.SetActive(true);
    }

    public void LoadTrainingMenu()
    {
        CloseAllMenus();
        trainingMenu.gameObject.SetActive(true);
    }

    public void LoadShopMenu()
    {
        CloseAllMenus();
        shopMenu.SetActive(true);
    }

    public void LoadPlayMenu()
    {
        CloseAllMenus();
        playMenu.SetActive(true);
    }

    public void LoadSettingsMenu()
    {
        CloseAllMenus();
        settingsMenu.SetActive(true);
    }

    public void LoadCreditsMenu()
    {
        CloseAllMenus();
        creditsMenu.SetActive(true);
    }

    public void LoadAddCashPopup()
    {
        addCashPopup.SetActive(true);
    }

    public void CloseAddCashPopup()
    {
        addCashPopup.SetActive(false);
    }

    public void LoadWaitingPopup()
    {
        waitingPopup.gameObject.SetActive(true);
    }

    public void LoadResetDataPopup()
    {
        resetDataPopup.gameObject.SetActive(true);
    }

    public void CloseResetDataPopup()
    {
        resetDataPopup.gameObject.SetActive(false);
    }

    public void ResetData()
    {
        PlayerPrefManager.ResetAllPrefs();
        ReloadAllMenus();
        LoadHomeMenu();
    }

    public void GiveAdReward()
    {
        int reward = EconomyManager.ShopAdReward;
        waitingPopup.Succeed("+" + reward + " C$");
        PlayerPrefManager.AddCash(reward);
        UpdateCashDisplay(true);
    }

    public void SucceedPurchaseCashReward(int reward)
    {
        waitingPopup.Succeed("+" + reward + " C$");
        PlayerPrefManager.AddCash(reward);
        UpdateCashDisplay(true);
    }

    public void FailWaitingPopup()
    {
        waitingPopup.Fail();
    }

    private void StartMatch(int enemyLeague, int enemyIndex)
    {
        AppManager.PlayerData = roster.Roster[PlayerPrefManager.GetSelectedCharacter()];
        AppManager.EnemyData = leagues[enemyLeague].League[enemyIndex];

        wiperAnimation.SetTrigger("wipe");

        StartCoroutine("LoadGameSceneWithDelay");
    }

    private IEnumerator LoadGameSceneWithDelay()
    {
        yield return new WaitForSeconds(0.8f);
        SceneManager.LoadScene(2);
    }

    public void PlayMusic(bool play)
    {
        menuMusic.enabled = play;
    }
}
