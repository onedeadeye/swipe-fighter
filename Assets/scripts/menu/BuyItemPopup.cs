using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyItemPopup : MonoBehaviour
{
    public delegate void GenericType();

    protected GenericType callbackFunction;

    [SerializeField]
    private Text nameText;
    [SerializeField]
    private Image iconImage;
    [SerializeField]
    private Text costText;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PromptItem(string itemName, Sprite itemSprite, int itemCost, GenericType callback)
    {
        nameText.text = "Purchase\n'" + itemName + "'?";
        iconImage.sprite = itemSprite;
        costText.text = "C$ " + itemCost;
        callbackFunction = callback;
        gameObject.SetActive(true);
    }

    public void ConfirmPrompt()
    {
        callbackFunction.Invoke();
        gameObject.SetActive(false);
    }

    public void CancelPrompt()
    {
        gameObject.SetActive(false);
    }
}
