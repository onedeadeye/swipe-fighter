using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TrainingMenu : MonoBehaviour
{
    [SerializeField]
    private Button boostTabButton;
    [SerializeField]
    private Button trainTabButton;

    [SerializeField]
    private GameObject boostTab;
    [SerializeField]
    private GameObject trainTab;

    [SerializeField]
    private Text strengthBoostText;
    [SerializeField]
    private Text agilityBoostText;
    [SerializeField]
    private Text defenseBoostText;

    [SerializeField]
    private Text strengthTrainText;
    [SerializeField]
    private Text agilityTrainText;
    [SerializeField]
    private Text defenseTrainText;

    [SerializeField]
    private Text strengthTrainCostText;
    [SerializeField]
    private Text agilityTrainCostText;
    [SerializeField]
    private Text defenseTrainCostText;

    void Start()
    {
        UpdateUI();
    }

    public void ShowBoostTab()
    {
        trainTabButton.interactable = true;
        boostTabButton.interactable = false;
        trainTab.SetActive(false);
        boostTab.SetActive(true);
    }

    public void ShowTrainTab()
    {
        boostTabButton.interactable = true;
        trainTabButton.interactable = false;
        boostTab.SetActive(false);
        trainTab.SetActive(true);
    }

    public void UpdateBoostText()
    {
        strengthBoostText.text = "+" + PlayerPrefManager.GetStrengthBoost() + "%";
        agilityBoostText.text = "+" + PlayerPrefManager.GetAgilityBoost() + "%";
        defenseBoostText.text = "+" + PlayerPrefManager.GetDefenseBoost() + "%";
    }

    public void UpdateTrainText()
    {
        strengthTrainText.text = "+" + PlayerPrefManager.GetStrengthTrain() + "%";
        agilityTrainText.text = "+" + PlayerPrefManager.GetAgilityTrain() + "%";
        defenseTrainText.text = "+" + PlayerPrefManager.GetDefenseTrain() + "%";
    }

    public void UpdateTrainCost()
    {
        strengthTrainCostText.text = "C$" + ((PlayerPrefManager.GetStrengthTrain() / 5) + 1) * 500;
        agilityTrainCostText.text = "C$" + ((PlayerPrefManager.GetAgilityTrain() / 5) + 1) * 500;
        defenseTrainCostText.text = "C$" + ((PlayerPrefManager.GetDefenseTrain() / 5) + 1) * 500;
    }

    public void RewardStrengthBoost(int amount)
    {
        PlayerPrefManager.AddStrengthBoost(amount);
        UpdateUI();
    }

    public void RewardAgilityBoost(int amount)
    {
        PlayerPrefManager.AddAgilityBoost(amount);
        UpdateUI();
    }

    public void RewardDefenseBoost(int amount)
    {
        PlayerPrefManager.AddDefenseBoost(amount);
        UpdateUI();
    }

    public void RewardStrengthTrain()
    {
        PlayerPrefManager.AddStrengthTrain(5);
        UpdateUI();
    }

    public void RewardAgilityTrain()
    {
        PlayerPrefManager.AddAgilityTrain(5);
        UpdateUI();
    }

    public void RewardDefenseTrain()
    {
        PlayerPrefManager.AddDefenseTrain(5);
        UpdateUI();
    }

    public void UpdateUI()
    {
        UpdateBoostText();
        UpdateTrainText();
        UpdateTrainCost();
        MenuManager.instance.UpdateCashDisplay(false);
    }
}
