using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenuManager : MonoBehaviour
{
    [SerializeField]
    MenuManager menuManager;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ToggleLowFramerateMode(bool newValue)
    {
        PlayerPrefManager.SetLowFramerateMode(newValue);
        Application.targetFrameRate = (PlayerPrefManager.GetLowFramerateMode() ? 30 : 60);
    }

    public void TogglePlayMusic(bool newValue)
    {
        PlayerPrefManager.SetPlayMusic(newValue);
        menuManager.PlayMusic(PlayerPrefManager.GetPlayMusic());
    }
}
