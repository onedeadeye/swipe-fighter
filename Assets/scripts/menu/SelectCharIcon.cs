using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectCharIcon : MonoBehaviour
{
    public Image thumbnail;
    public GameObject selectedIcon;
    public GameObject lockedIcon;
}
