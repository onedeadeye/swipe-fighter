using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroManager : MonoBehaviour
{
    [SerializeField]
    private GameObject touchEffect;
    private bool acceptingTouch = false;
    private Animator animator;

    void Awake()
    {
        Application.targetFrameRate = (PlayerPrefManager.GetLowFramerateMode() ? 30 : 60);
    }

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (acceptingTouch)
        {
            if (Input.touchCount > 0)
            {
                ShowEffectAndPlay(Camera.main.ScreenToWorldPoint(Input.touches[0].position));
            } else if (Input.GetMouseButtonDown(0)) {
                ShowEffectAndPlay(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            }
        }
    }

    public void ActivateTouch()
    {
        acceptingTouch = true;
    }

    public void LoadMenu()
    {
        AppManager.PlayMenuIntro = true;
        SceneManager.LoadScene(1);
    }

    private void ShowEffectAndPlay(Vector3 location) {  
        location.z = -6f;
        GameObject newEffect = GameObject.Instantiate(touchEffect, location, Quaternion.identity);
        //Destroy(newEffect, 0.5f);
        acceptingTouch = false;
        animator.SetTrigger("play");
    }
}
