using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitingPopup : MonoBehaviour
{
    [SerializeField]
    private Text statusText;

    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void Succeed()
    {
        statusText.text = "Success!";
        animator.SetTrigger("succeed");
        StartCoroutine("QueueSelfHide");
    }

    public void Succeed(string text)
    {
        statusText.text = text;
        animator.SetTrigger("succeed");
        StartCoroutine("QueueSelfHide");
    }

    public void Fail()
    {
        statusText.text = "Failed.";
        animator.SetTrigger("fail");
        StartCoroutine("QueueSelfHide");
    }

    private IEnumerator QueueSelfHide()
    {
        yield return new WaitForSeconds(2f);
        Reset();
        gameObject.SetActive(false);
    }

    private void Reset()
    {
        statusText.text = "Waiting...";
        animator.SetTrigger("reset");
    }
}
