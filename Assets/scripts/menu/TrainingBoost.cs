using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TrainingBoost : MonoBehaviour
{
    [SerializeField]
    private int purchaseCost;
    [SerializeField]
    private Text nameText;
    [SerializeField]
    private Text costText;
    [SerializeField]
    private Image image;

    [SerializeField]
    private UnityEvent onPurchase;

    // Start is called before the first frame update
    void Start()
    {
        costText.text = "C$ " + purchaseCost;
    }

    public void PurchaseBoost()
    {
        if (PlayerPrefManager.GetCash() < purchaseCost)
        {
            MenuManager.instance.LoadAddCashPopup();
        }
        else
        {
            MenuManager.instance.buyItemPopup.PromptItem(nameText.text, image.sprite, purchaseCost, CompletePurchase);
        }
    }

    public void CompletePurchase()
    {
        PlayerPrefManager.LoseCash(purchaseCost);
        onPurchase.Invoke();
    }
}
