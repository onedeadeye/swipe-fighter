using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TrainingUpgrade : MonoBehaviour
{
    [SerializeField]
    private Text nameText;
    [SerializeField]
    private Text costText;
    [SerializeField]
    private Image image;

    int purchaseCost = 0;

    [SerializeField]
    private UnityEvent onPurchase;

    public void PurchaseStrTrain()
    {
        purchaseCost = ((PlayerPrefManager.GetStrengthTrain() / 5) + 1) * 500;
        if (PlayerPrefManager.GetCash() < purchaseCost)
        {
            MenuManager.instance.LoadAddCashPopup();
        }
        else
        {
            MenuManager.instance.buyItemPopup.PromptItem(nameText.text, image.sprite, purchaseCost, CompletePurchase);
        }
    }

    public void PurchaseAgiTrain()
    {
        purchaseCost = ((PlayerPrefManager.GetAgilityTrain() / 5) + 1) * 500;
        if (PlayerPrefManager.GetCash() < purchaseCost)
        {
            MenuManager.instance.LoadAddCashPopup();
        }
        else
        {
            MenuManager.instance.buyItemPopup.PromptItem(nameText.text, image.sprite, purchaseCost, CompletePurchase);
        }
    }

    public void PurchaseDefTrain()
    {
        purchaseCost = ((PlayerPrefManager.GetDefenseTrain() / 5) + 1) * 500;
        if (PlayerPrefManager.GetCash() < purchaseCost)
        {
            MenuManager.instance.LoadAddCashPopup();
        }
        else
        {
            MenuManager.instance.buyItemPopup.PromptItem(nameText.text, image.sprite, purchaseCost, CompletePurchase);
        }
    }

    public void CompletePurchase()
    {
        PlayerPrefManager.LoseCash(purchaseCost);
        onPurchase.Invoke();
    }
}
