using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Cinemachine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    private static TouchInputManager touchInputInstance;
    private static Animator animator;

    [Header("Debug Elements")]
    [SerializeField]
    private CharacterData debugPlayer;
    [SerializeField]
    private MatchData debugEnemy;

    [Header("Control Elements")]
    [SerializeField]
    private CinemachineTargetGroup cvTargetGroup;

    [SerializeField]
    private Player player;
    [SerializeField]
    private Enemy enemy;

    private CharacterData playerData;
    private MatchData enemyData;

    public GameObject hitEffectPrefab;

    [Header("Ingame Elements")]
    [SerializeField]
    private RectTransform playerHealthMeter;
    [SerializeField]
    private Text playerNameText;
    [SerializeField]
    private Image playerProfilePic;

    [SerializeField]
    private SpriteRenderer[] playerWinMarkers;

    [SerializeField]
    private RectTransform enemyHealthMeter;
    [SerializeField]
    private Text enemyNameText;
    [SerializeField]
    private Image enemyProfilePic;

    // Round win indicator elements
    [Header("Win Indicator Elements")]
    [SerializeField]
    private SpriteRenderer[] enemyWinMarkers;

    [SerializeField]
    private Sprite playerWinSprite;
    [SerializeField]
    private Sprite enemyWinSprite;

    [Header("Postgame Elements")]
    [SerializeField]
    private SpriteRenderer postgameResultImage;
    [SerializeField]
    private Text postgameAttacksText;
    [SerializeField]
    private Text postgameDamageText;
    [SerializeField]
    private Text postgameRewardText;

    [SerializeField]
    private Sprite postgameWinImage;
    [SerializeField]
    private Sprite postgameLoseImage;

    private bool flippedNormal = true;

    // game settings are here for now
    private int roundsToWin = 3;

    private float unfreezeTime = 0f;
    private float minFreezeDamage = 20f;
    private float minFreezeTime = 0.1f;

    // play statistics
    private int attacksHit = 0;
    private int attacksMissed = 0;
    private float damageDealt = 0f;
    private float damageReceived = 0f;

    // variables
    private float fighterDistance = 0f;
    private float minimumDistance = 0f;

    private int playerRoundWins = 0;
    private int enemyRoundWins = 0;

    private int rewardCash = 0;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        touchInputInstance = GetComponent<TouchInputManager>();
        animator = GetComponent<Animator>();

        if (AppManager.PlayerData != null)
        {
            playerData = AppManager.PlayerData;
        }
        else
        {
            playerData = debugPlayer;
        }

        if (AppManager.EnemyData != null)
        {
            enemyData = AppManager.EnemyData;
        }
        else
        {
            enemyData = debugEnemy;
        }

        player = Instantiate(playerData.prefab).GetComponent<Player>();

        enemy = Instantiate(enemyData.prefab).GetComponent<Enemy>();

        playerNameText.text = playerData.Name;
        enemyNameText.text = enemyData.Name;

        playerProfilePic.sprite = playerData.thumbnail;
        enemyProfilePic.sprite = enemyData.thumbnail;

        enemy.SetFlipped(true);
        enemy.difficulty = enemyData.difficulty;
        enemy.boosts = enemyData.boosts;

        player.boosts = PlayerPrefManager.GetTotalStatChanges();
        PlayerPrefManager.SetZeroBoosts();

        cvTargetGroup.AddMember(player.transform, 1f, 1.5f);
        cvTargetGroup.AddMember(enemy.transform, 1f, 1.5f);

        minimumDistance = (player.width / 2) + (enemy.width);

        animator.SetTrigger("intro");
    }

    // FixedUpdate isn't called while time is stopped, so this has to go here.
    void Update()
    {
        if (Time.unscaledTime < unfreezeTime)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }

    void FixedUpdate()
    {
        fighterDistance = Mathf.Abs(player.gameObject.transform.position.x - enemy.gameObject.transform.position.x);

        if (player.gameObject.transform.position.x > enemy.gameObject.transform.position.x && flippedNormal)
        {
            player.SetFlipped(true);
            enemy.SetFlipped(false);

            flippedNormal = false;
        }
        else if (player.gameObject.transform.position.x < enemy.gameObject.transform.position.x && !flippedNormal)
        {
            player.SetFlipped(false);
            enemy.SetFlipped(true);

            flippedNormal = true;
        }

        if (fighterDistance < minimumDistance)
        {
            player.SetMoveTarget(enemy.transform.position.x - (minimumDistance * (player.Flipped ? -1 : 1)));
        }

        if (fighterDistance > minimumDistance * 1.25)
        {
            player.SetMoveTarget(enemy.transform.position.x - (minimumDistance * (player.Flipped ? -1 : 1)));
        }
    }

    public static TouchInputManager GetTouchInputInstance()
    {
        return touchInputInstance;
    }

    public void UpdateHealthDisplays()
    {
        // these two are different because They're Different
        // also, this is where we check for round wins

        float playerHealthFraction = player.currentHealth / player.maxHealth;
        // 1050 is the width of this UI element currently. it'll probably change, and this will break.
        playerHealthMeter.sizeDelta = new Vector2((1 - playerHealthFraction) * -1050, 0);

        float enemyHealthFraction = enemy.currentHealth / enemy.maxHealth;
        enemyHealthMeter.sizeDelta = new Vector2((1 - enemyHealthFraction) * -1050, 0);
    }

    public void AlertWin(bool playerWon)
    {
        EndRound(playerWon);
    }

    public void ResetForNewRound()
    {
        player.Reset();
        enemy.Reset();

        UpdateHealthDisplays();

        animator.SetTrigger("intro");
    }

    public void StartRound()
    {
        player.entityActive = true;
        enemy.entityActive = true;
    }

    private void EndRound(bool playerWon)
    {
        player.entityActive = false;
        enemy.entityActive = false;

        if (playerWon)
        {
            playerRoundWins++;
            playerWinMarkers[playerRoundWins - 1].sprite = playerWinSprite;
            if (playerRoundWins == roundsToWin)
            {
                animator.SetTrigger("matchWin");
                SetPostgameUI(true);
            }
            else
            {
                animator.SetTrigger("roundWin");
            }
        }
        else
        {
            enemyRoundWins++;
            enemyWinMarkers[enemyRoundWins - 1].sprite = enemyWinSprite;
            if (enemyRoundWins == roundsToWin)
            {
                animator.SetTrigger("matchLoss");
                SetPostgameUI(false);
            }
            else
            {
                animator.SetTrigger("roundLoss");
            }
        }
    }

    private void SetPostgameUI(bool playerWon)
    {
        rewardCash = playerWon ? EconomyManager.VictoryReward : 0;
        rewardCash += Mathf.RoundToInt(attacksHit / (attacksHit + attacksMissed) * EconomyManager.MaxOffenseBonus);
        rewardCash += Mathf.RoundToInt(damageDealt / (damageDealt + damageReceived) * EconomyManager.MaxDefenseBonus);
        rewardCash = (int)rewardCash;

        postgameResultImage.sprite = playerWon ? postgameWinImage : postgameLoseImage;
        postgameAttacksText.text = attacksHit + " hits of\n" + (attacksHit + attacksMissed) + " attacks";
        postgameDamageText.text = (int)damageReceived + " damage taken\n" + (int)damageDealt + " damage dealt";
        postgameRewardText.text = rewardCash + " cash earned";
    }

    public void AlertAttack(bool hit)
    {
        if (hit)
        {
            //Debug.Log("attack hit");
            attacksHit++;
        }
        else
        {
            //Debug.Log("attack missed");
            attacksMissed++;
        }
    }

    public void AlertDamage(float damage, bool isPlayer)
    {
        // If the enemy is reporting that it took damage, increase our total damage done
        if (!isPlayer)
        {
            damageDealt += damage;
            player.PlayAttackSound();
        }
        else
        {
            damageReceived += damage;
            enemy.PlayAttackSound();
        }

        if (damage >= minFreezeDamage)
        {
            unfreezeTime = Time.unscaledTime + ((damage / minFreezeDamage) * minFreezeTime);
        }
    }

    public Player GetPlayer()
    {
        return player;
    }

    public void EndGameScene(bool bonusRewards)
    {
        PlayerPrefManager.AddCash((rewardCash * (bonusRewards ? EconomyManager.PostgameAdMultiplier : 1)));
        AppManager.PlayMenuIntro = false;
        SceneManager.LoadSceneAsync(1);
    }
}
