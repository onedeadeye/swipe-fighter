using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerPrefManager
{
    private static string unlockCharacterPrefix = "unlockCharacter";

    private static string selectedCharacterString = "selectedCharacter";
    private static int defaultSelectedCharacterIndex = 0;

    private static string cashString = "cash";
    private static int defaultCash = 0;

    private static string strengthBoostString = "strBoost";
    private static string agilityBoostString = "agiBoost";
    private static string defenseBoostString = "defBoost";

    private static string strengthTrainString = "strTrain";
    private static string agilityTrainString = "agiTrain";
    private static string defenseTrainString = "defTrain";

    public static string playMusicString = "playMusic";
    public static string lowFramerateModeString = "lowFramerateMode";

    public static bool GetUnlockedCharacter(int index)
    {
        return PlayerPrefs.GetInt(unlockCharacterPrefix + index, 0) == 1;
    }

    public static void UnlockCharacter(int index)
    {
        PlayerPrefs.SetInt(unlockCharacterPrefix + index, 1);
    }

    public static int GetSelectedCharacter()
    {
        return PlayerPrefs.GetInt(selectedCharacterString, defaultSelectedCharacterIndex);
    }

    public static void SetSelectedCharacter(int index)
    {
        PlayerPrefs.SetInt(selectedCharacterString, index);
    }

    public static int GetCash()
    {
        return PlayerPrefs.GetInt(cashString, defaultCash);
    }

    public static void AddCash(int add)
    {
        PlayerPrefs.SetInt(cashString, GetCash() + add);
    }

    public static void LoseCash(int lose)
    {
        PlayerPrefs.SetInt(cashString, GetCash() - lose);
    }

    // Stat boosts are stored as int. 1 = 1% boost, 2 = 2% boost, etc.
    public static int GetStrengthBoost()
    {
        return PlayerPrefs.GetInt(strengthBoostString, 0);
    }

    public static void AddStrengthBoost(int add)
    {
        PlayerPrefs.SetInt(strengthBoostString, GetStrengthBoost() + add);
    }

    public static int GetAgilityBoost()
    {
        return PlayerPrefs.GetInt(agilityBoostString, 0);
    }

    public static void AddAgilityBoost(int add)
    {
        PlayerPrefs.SetInt(agilityBoostString, GetAgilityBoost() + add);
    }

    public static int GetDefenseBoost()
    {
        return PlayerPrefs.GetInt(defenseBoostString, 0);
    }

    public static void AddDefenseBoost(int add)
    {
        PlayerPrefs.SetInt(defenseBoostString, GetDefenseBoost() + add);
    }

    public static Vector3 GetTotalStatChanges()
    {
        Vector3 boosts = new Vector3(GetStrengthBoost(), GetAgilityBoost(), GetDefenseBoost());
        Vector3 trainings = new Vector3(GetStrengthTrain(), GetAgilityTrain(), GetDefenseTrain());
        return boosts + trainings;
    }

    public static void SetZeroBoosts()
    {
        PlayerPrefs.SetInt(strengthBoostString, 0);
        PlayerPrefs.SetInt(agilityBoostString, 0);
        PlayerPrefs.SetInt(defenseBoostString, 0);
    }

    public static int GetStrengthTrain()
    {
        return PlayerPrefs.GetInt(strengthTrainString, 0);
    }

    public static void AddStrengthTrain(int add)
    {
        PlayerPrefs.SetInt(strengthTrainString, GetStrengthTrain() + add);
    }

    public static int GetAgilityTrain()
    {
        return PlayerPrefs.GetInt(agilityTrainString, 0);
    }

    public static void AddAgilityTrain(int add)
    {
        PlayerPrefs.SetInt(agilityTrainString, GetAgilityTrain() + add);
    }

    public static int GetDefenseTrain()
    {
        return PlayerPrefs.GetInt(defenseTrainString, 0);
    }

    public static void AddDefenseTrain(int add)
    {
        PlayerPrefs.SetInt(defenseTrainString, GetDefenseTrain() + add);
    }

    // SETTINGS

    public static bool GetPlayMusic()
    {
        return PlayerPrefs.GetInt(playMusicString, 1) == 1;
    }

    public static void SetPlayMusic(bool play)
    {
        PlayerPrefs.SetInt(playMusicString, play ? 1 : 0);
    }

    public static bool GetLowFramerateMode()
    {
        return PlayerPrefs.GetInt(lowFramerateModeString, 0) == 1;
    }

    public static void SetLowFramerateMode(bool active)
    {
        PlayerPrefs.SetInt(lowFramerateModeString, active ? 1 : 0);
    }

    // RESET

    public static void ResetAllPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}
