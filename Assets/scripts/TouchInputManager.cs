using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class TouchInputManager : MonoBehaviour
{
    private Vector2 swipeStartPosition;
    private Vector2 swipeEndPosition;

    [SerializeField]
    private float minDistanceForSwipe = 20f;

    public static event Action<SwipeDirection> OnSwipe = delegate { };

    public static bool PlayerFlipped = false;

    // Update is called once per frame
    void Update()
    {
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                swipeStartPosition = touch.position;
                swipeEndPosition = touch.position;
            }

            if (touch.phase == TouchPhase.Ended)
            {
                swipeEndPosition = touch.position;
                DetectSwipe();
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            swipeStartPosition = Input.mousePosition;
            swipeEndPosition = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            swipeEndPosition = Input.mousePosition;
            DetectSwipe();
        }
    }

    private void DetectSwipe()
    {
        if (SwipeLongEnough())
        {
            float deltaX = swipeEndPosition.x - swipeStartPosition.x;
            float deltaY = swipeEndPosition.y - swipeStartPosition.y;

            deltaX = deltaX * (PlayerFlipped ? -1 : 1);

            double angle = Math.Atan2(deltaY, deltaX) * (180 / Math.PI);

            // this is where you convert angle to a SwipeDirection
            SwipeDirection swipeDirection = ConvertSwipe(angle);

            OnSwipe.Invoke(swipeDirection);
        }
    }

    private bool SwipeLongEnough()
    {
        return VerticalSwipeLength() > minDistanceForSwipe || HorizontalSwipeLength() > minDistanceForSwipe;
    }

    private float VerticalSwipeLength()
    {
        return Mathf.Abs(swipeStartPosition.y - swipeEndPosition.y);
    }

    private float HorizontalSwipeLength()
    {
        return Mathf.Abs(swipeStartPosition.x - swipeEndPosition.x);
    }

    private SwipeDirection ConvertSwipe(double angle)
    {
        int number = ((int)Math.Round(angle / 45));
        switch (number)
        {
            // you're right, this *could* be cleaner with better math!
            // it will never be, however.
            case 0: return SwipeDirection.East;
            case 1: return SwipeDirection.NorthEast;
            case 2: return SwipeDirection.North;
            case 3: return SwipeDirection.NorthWest;
            case 4:
            case -4: return SwipeDirection.West;
            case -3: return SwipeDirection.SouthWest;
            case -2: return SwipeDirection.South;
            case -1: return SwipeDirection.SouthEast;
            default: throw (new ApplicationException());
        }
    }

    private bool AngleInRange(double angle, float upperBound, float lowerBound)
    {
        return (angle > lowerBound && angle < upperBound);
    }

    public enum SwipeDirection
    {
        None,
        North,
        NorthEast,
        East,
        SouthEast,
        South,
        SouthWest,
        West,
        NorthWest
    }
}
