public static class EconomyManager
{
    // Advertisement 
    public static int ShopAdReward = 200;
    public static int PostgameAdMultiplier = 2;

    // Postgame rewards
    public static int VictoryReward = 75;
    public static int MaxOffenseBonus = 25;
    public static int MaxDefenseBonus = 25;
}
